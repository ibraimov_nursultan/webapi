﻿using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
using Newtonsoft.Json;
using System;
using System.Linq;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly RGDialogsClients r = new RGDialogsClients();

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(r.Init());
        }

        [HttpGet("search")]
        public IActionResult Search(string IDClient, Guid IDDialog)
        {
            try
            {
                RGDialogsClients n = null;
                if (IDClient != null)
                {
                    string[] clients = IDClient.Split(',');
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string json = JsonConvert.SerializeObject(clients);
                    Guid[] clientId = js.Deserialize<Guid[]>(json);
                    foreach (Guid g in clientId)
                    {
                        n = r.Init().FirstOrDefault(p => p.IDClient == g);
                        if (n == null || n.IDRGDialog != IDDialog)
                        {
                            n = null;
                            break;
                        }
                    }
                }
                if (n != null)
                    return Ok(n.IDRGDialog);
                else
                    return Ok(new Guid());
            }
            catch
            {
                return Ok("Error");
            }
        }

    }
}
